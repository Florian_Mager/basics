package at.mager.remote;

import at.mager.remote.Device.type;

public class RemoteStarter {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Remote r1 = new Remote(60,20,300,"7B");
		Remote r2 = r1;
		
		Device d1 = new Device("1234jk", Device.type.beamer);
		Device d2 = new Device("555", Device.type.rollo);
		
		r1.addDevice(d1);
		r1.addDevice(d2);
		System.out.println(r1.getDevices().get(1).getSerialNumber());
		
		r1.sayHello();
		r2.sayHello();
		r1.Home();
		
	}

}
