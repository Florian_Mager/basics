package at.mager.remote;

import java.util.ArrayList;
import java.util.List;

public class Remote {

	private int length;
	private int width;
	private int weight;
	private String number;
	private boolean isOn;
	private List<Device> devices;
	
	public Remote(int length, int width, int weight, String number) {
		super();
		this.length = length;
		this.width = width;
		this.weight = weight;
		this.number = number;
		this.devices = new ArrayList<>();
	}
	
	public List<Device> getDevices() {
		return devices;
	}

	public void setDevices(List<Device> devices) {
		this.devices = devices;
	}

	public void addDevice(Device device) {
		this.devices.add(device);
	}
	
	public void sayHello() {
		System.out.println("I have the following values -->" + " Number: " + this.number + " Length: " + this.length + ", Width: " + this.width + " Weight: " + this.weight);
	}
	
	public void Home() {
		System.out.println("Hello, how can I help you?");
	}
	
	public void turnOn() {
		System.out.println("I am turned on now");
		
	}
	
	public void turnOff() {
		System.out.println("I am turned off now");
	}
	
}