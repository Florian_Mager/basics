package at.mager.cars;

public class Hersteller {

	private String herstellername;
	private String herkunftsland;
	private int rabatt;

	public Hersteller(String herstellername, String herkunftsland, int rabatt) {
		this.herstellername = herstellername;
		this.herkunftsland = herkunftsland;
		this.rabatt = rabatt;
	}

	public String getHerstellername() {
		return herstellername;
	}

	public void setHerstellername(String herstellername) {
		this.herstellername = herstellername;
	}

	public String getHerkunftsland() {
		return this.herkunftsland;
	}

	public double getRabatt() {
		return this.rabatt;
	}

}