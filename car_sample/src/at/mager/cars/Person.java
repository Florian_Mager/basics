package at.mager.cars;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;

public class Person {
	private String firstName;
	private String lastName;
	private LocalDate birthday;
	private List<Auto> autos;

	public Person(String firstName, String lastName, LocalDate birthday) {

		this.firstName = firstName;
		this.lastName = lastName;
		this.birthday = birthday;
		this.autos = new ArrayList<>();
	}

	public int getAge() {
		LocalDate date = LocalDate.now();
		return this.birthday.until(date).getYears();
	}

	public double getValueOfCars() {

		double result = 0;
		for (Auto auto : autos) {
			result = result + auto.getPreis();
		}
		return result;
	}

	public void addCar(Auto cars) {
		this.autos.add(cars);
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public LocalDate getBirthday() {
		return birthday;
	}

	public void setBirthday(LocalDate birthday) {
		this.birthday = birthday;
	}

	public List<Auto> getCars() {
		return autos;
	}

	public void setCars(List<Auto> cars) {
		this.autos = cars;
	}

}