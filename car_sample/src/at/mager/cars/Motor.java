package at.mager.cars;

public class Motor {

	private String dieselbenzin;
	private int leistung;

	public Motor(String string, int leistung) {
		this.dieselbenzin = string;
		this.leistung = leistung;
	}

	public String getDieselbenzin() {
		return dieselbenzin;
	}

	public void setDieselbenzin(String dieselbenzin) {
		this.dieselbenzin = dieselbenzin;
	}

	public int getLeistung() {
		return leistung;
	}

}