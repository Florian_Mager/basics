package at.mager.cars;

import java.time.LocalDate;

import at.mager.remote.Device;

public class AutoStarter {

	public static void main(String[] args) {

		Motor m1 = new Motor("diesel", 1000);
		Motor m2 = new Motor("benzin", 1000);
		Motor m3 = new Motor("elektro", 1000);

		Hersteller h1 = new Hersteller("vw", "Österreich", 10);
		Hersteller h2 = new Hersteller("mercedes", "Österreich", 10);
		Hersteller h3 = new Hersteller("bmw", "Österreich", 10);

		Auto a1 = new Auto("white", 200, 30000, 30000, 0.00, 0.00, m1, h1);
		Auto a2 = new Auto("black", 200, 30000, 30000, 0.00, 0.00, m2, h2);
		Auto a3 = new Auto("purple", 200, 30000, 30000, 0.00, 0.00, m3, h3);

		Person p1 = new Person("Florian", "Mager", LocalDate.of(2000, 12, 03));

		p1.addCar(a1);
		p1.addCar(a2);
		p1.addCar(a3);

		System.out.println("Mein Name ist " + p1.getFirstName() + " " + p1.getLastName() + ".");

		System.out.println("Ich bin " + p1.getAge() + " Jahre alt.");

		System.out.println("Meine Autos sind insgesamt " + p1.getValueOfCars() + "€ wert.");

//		System.out.println(a1.getEngine().getDieselbenzin());
//		System.out.println(a1.getProducer().getHerstellername());

//		Hersteller VW = new Hersteller("VW", "Österreich", 15);
//		a1.preis = a1.getPrice() - a1.getPrice() / 100 * VW.getRabatt();
//		System.out.println("Preis (mit abgezogenem Rabatt): " + a1.preis);
//		a1.verbrauch2 = a1.getVerbrauch() + (a1.getVerbrauch() / 100 * 9.8);
//		System.out.println("Verbrauch: " + a1.verbrauch1 + " unter 50000 km");
//		System.out.println("Verbrauch: " + a1.verbrauch2 + " ueber 50000 km");
//		Motor vwmotor = new Motor("diesel", 150);

	}

}