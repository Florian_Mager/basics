package at.mager.cars;

import java.util.ArrayList;
import java.util.List;

public class Auto {

	private String color;
	private int speedMax;
	private double price;
	private double basicPrice;
	private double consumption1;
	private double consumption2;
	private double skonto;
	private Motor engine;
	private Hersteller producer;
	private List<Person> persons;

	public Auto(String color, int speedMax, double price, double basicPrice, double consumption1, double consumption2,
			Motor engine, Hersteller producer) {
		this.color = color;
		this.speedMax = speedMax;
		this.price = price;
		this.basicPrice = basicPrice;
		this.consumption1 = consumption1;
		this.consumption2 = consumption2;
		this.engine = engine;
		this.producer = producer;
		this.persons = new ArrayList<>();
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getSpeedmax() {
		return speedMax;
	}

	public void setSpeedmax(int speedmax) {
		this.speedMax = speedmax;
	}

	public double getPreis() {
		return price;
	}

	public void setPreis(double price) {
		this.price = price;
	}

	public double getBasicprice() {
		return basicPrice;
	}

	public void setBasicPrice(double basicprice) {
		this.basicPrice = basicprice;
	}

	public double getConsumption1() {
		return consumption1;
	}

	public void setConsumption1(double consumption1) {
		this.consumption1 = consumption1;
	}

	public double getConsumption2() {
		return consumption2;
	}

	public void setConsumption2(double consumption2) {
		this.consumption2 = consumption2;
	}

	public double getSkonto() {
		return skonto;
	}

	public void setSkonto(double skonto) {
		this.skonto = skonto;
	}

	public List<Person> getPersons() {
		return persons;
	}

	public void setPersons(List<Person> persons) {
		this.persons = persons;
	}

	public Motor getEngine() {
		return engine;
	}

	public void setEngine(Motor engine) {
		this.engine = engine;
	}

	public Hersteller getProducer() {
		return producer;
	}

	public void setProducer(Hersteller producer) {
		this.producer = producer;
	}

}
